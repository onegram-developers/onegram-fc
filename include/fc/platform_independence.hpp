#ifdef _MSC_VER
#include <intrin.h>
    #ifdef _M_X64
    #define __builtin_popcountll __popcnt64
    #else
    inline int __builtin_popcountll(unsigned __int64 value)
    {
       unsigned int lowBits = (unsigned int)value;
       int count = __popcnt(lowBits);
       unsigned int highBits = (unsigned int)(value >> 32);
       count += __popcnt(highBits);
       return count;
    }
    #endif
#endif

// Note that clang also defines __GNU*__ and thus must precede gcc.

#if __cplusplus >= 201703L
        #define FALLTHROUGH [[fallthrough]];
#else
    #if __clang__
        #define FALLTHROUGH [[clang::fallthrough]];
    #elif __GNUC__ || __GNUG__
        #define FALLTHROUGH [[gnu::fallthrough]];
    #else
        #define FALLTHROUGH
    #endif
#endif

#if __cplusplus >= 201703L
        #define UNUSED [[maybe_unused]]
#else
    #if __clang__
        #define UNUSED [[maybe_unused]]
    #elif __GNUC__ || __GNUG__
        #define UNUSED [[gnu::unused]]
    #else
        #define UNUSED
    #endif
#endif

